#!/usr/bin/env python

import asyncio
import fabapi
import fabapi.user_system

async def main():
    session = await fabapi.connect("localhost", 59661, "Testuser", "secret")

    # Add an user with the given roles
    roles = [
        "somerole", "testrole"
    ]
    user = await fabapi.user_system.add_user(session.userSystem, "ANewUser", "ANewSecret", roles=roles)
    print("ANewUser obj: ", user)

    # As you can see, Roles were attached
    r = await user.info.listRoles().a_wait()
    print("ANewUser roles: ", r)

    # Delete the same user again
    await fabapi.user_system.del_user(session.userSystem, "ANewUser")

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
