#!/usr/bin/env python

import asyncio
import fabapi
import fabapi.user_system

async def main():
    session = await fabapi.connect("localhost", 59661, "Testuser", "secret")
    ma = await session.machineSystem.info.getMachineURN("urn:fabaccess:resource:Another").a_wait()
    await ma.just.use.use().a_wait()

    ma = await session.machineSystem.info.getMachineURN("urn:fabaccess:resource:Another").a_wait()
    await ma.just.inuse.sendRawData(b"FooBarData").a_wait()

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
