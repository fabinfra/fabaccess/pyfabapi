#!/usr/bin/env python

import asyncio
import fabapi
import fabapi.user_system

async def main():
    session = await fabapi.connect("localhost", 59661, "Testuser", "secret")
    info = session.machineSystem.info
    ma = await info.getMachineURN("urn:fabaccess:resource:Another").a_wait()
    # ma.just isn't set if the above selected resource doesn't exist
    if ma.just:
        await ma.just.use.use().a_wait()
        # Do whatever

        # To give the resource back we have to re-query the changed state
        ma = await info.getMachineURN("urn:fabaccess:resource:Another").a_wait()
        await ma.just.inuse.giveBack().a_wait()
    else:
        print("No such resource!")

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
