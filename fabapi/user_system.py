import capnp
from .schema import usersystem_capnp
from .schema import role_capnp

async def add_user(usersystem, username, password, roles=None):
    """
    Add a new user with a given username and password.

    If provided a given list of roles is granted to the newly created user.
    Returns the new user
    """

    new_user_fallible = await usersystem.manage.addUserFallible(username, password).a_wait()
    if new_user_fallible.which._str() == "successful":
        new_user = new_user_fallible.successful
        if roles:
            for r in roles:
                new_user.admin.addRole(role_capnp.Role(name=r))
        return new_user
    else:
        return new_user_fallible.failed.error

async def del_user(usersystem, username):
    user_optional = await usersystem.search.getUserByName(username).a_wait()
    if user_optional.which._str() == "just":
        user = user_optional.just
        await usersystem.manage.removeUser(user).a_wait()
