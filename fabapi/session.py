class Session:
    """
    A connected API session

    Implements housekeeping functionality around conneciton keep-alive and re-establishment.
    """

    session = None

    def __init__(self, session):
        self.session = session