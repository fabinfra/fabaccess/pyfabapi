# FabAccess for Python

Small library to give access to the FabAccess API for administration and development

## Documentation

Information about installation, usage and tips and tricks can be found at [docs.fab-access.org](https://docs.fab-access.org/books/schnittstellen-und-apis/page/fabaccess-api#bkmrk-pyfabapi).